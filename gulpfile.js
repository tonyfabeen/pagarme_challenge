'use strict';

var gulp   = require('gulp'),
    gutil  = require('gulp-util'),
    jshint = require('gulp-jshint'),
    mocha  = require('gulp-mocha');

gulp.task('default', ['watch']);

gulp.task('jshint', function() {
  return gulp.src('{lib, test}/**/*.js')
             .pipe(jshint())
             .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('test', function() {
  return gulp.src('test/**/*.js')
             .pipe(mocha({ reporter: 'spec'}));
});

gulp.task('watch', function() {
  return gulp.watch('**/**/*.js', ['jshint', 'test']);
});


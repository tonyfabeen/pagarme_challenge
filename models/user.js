'use strict';
module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    login: DataTypes.STRING,
    avatar: DataTypes.STRING,
    blog: DataTypes.STRING
  });

  return User;
};

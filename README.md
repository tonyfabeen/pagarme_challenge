# Pagar.me Challenge

Essa lib comtempla a busca de um usuário do Github recebendo como argumento o **login** do mesmo.

### Dependencias
  - NodeJS: 5.1.0
  - NPM: 3.3.12

  Antes de executar o script é necessário atualizar suas dependencias internas executando :
```sh
$ npm install
```
## Executando o script
```sh
$ npm run execute --login <login_do_usuario_no_github>
```

## Executando a suíte de testes
```sh
$ npm test
```
## Autor
Tony Fabeen Oreste <<tony.fabeen@gmail.com>>


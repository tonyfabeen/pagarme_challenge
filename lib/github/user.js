'use strict';

var request = require('superagent');

const URL = 'https://api.github.com/users/';

class User {
  constructor(attributes) {
    this.login      = attributes.login,
    this.avatar     = attributes.avatar_url,
    this.blog       = attributes.blog
  }

  static find(name) {
    return new Promise((resolve, reject) => {
      var handleResponse = () => {
        return (error, response) => {
          if (error) return reject(error);

          return resolve(new User(response.body));
        };
      };

      request.get(URL + name)
             .type('json')
             .set('Accept', 'application/json')
             .end(handleResponse());
    });
  }
}

module.exports = User;

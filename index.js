var commandLine    = require('commander'),
    models         = require('./models'),
    githubUsersApi = require('./lib/github/user'),
    UserDomain     = require('./lib/domain/user'),
    user           = new UserDomain(models.User, githubUsersApi);

commandLine
  .version('0.0.1')
  .option('-l --login', 'Search for Login')
  .parse(process.argv);

if (!commandLine.args.length) {
  console.log('Wrong Arguments', commandLine.help());
  process.exit(1);
}

var login = commandLine.args.shift();

models.sequelize.sync().then(() => {
  console.log(`Looking for ${login} ...\n`);

  user.findByLogin(login)
      .then((user) =>  {
        console.log('User found!\n');
        console.log(`LOGIN: ${user.login}`);
        console.log(`AVATAR: ${user.avatar}`);
        console.log(`BLOG URL: ${user.blog}`);
      })
      .catch((error) => {
        if (error.status == 404) {
          console.log('User NOT Found!');
        } else {
          console.log('Something went wrong', error)
        }
  });
});


'use strict';

var chai           = require('chai'),
    chaiAsPromised = require('chai-as-promised'),
    expect         = chai.expect,
    UserApi        = require('../../lib/github/user');


describe('lib/github/user', () => {

  beforeEach(() => chai.use(chaiAsPromised));

  describe('.constructor', () => {
    var attributes = { login: 'some_login',
                       avatar_url: 'some_avatar',
                       blog: 'some_blog'},
        subject    = new UserApi(attributes);

    it('login will be initialized', () => {
      expect(subject.login).to.be.equal('some_login');
    });

    it('avatar will be initialized', () => {
      expect(subject.avatar).to.be.equal('some_avatar');
    });

    it('blog will be initialized', () => {
      expect(subject.blog).to.be.equal('some_blog');
    });
  });

  describe('.find', () => {
    it('returns a user', () => {
      return expect(UserApi.find('tonyfabeen'))
             .to
             .eventually
             .be
             .an
             .instanceof(UserApi);
    });
  });
});

'use strict';

class User {
  constructor(model, api) {
    this._model = model;
    this._api   = api;
  }

  _findOnDb(login) {
    return this._model.findAll({ where: { login: login} });
  }

  _findOnApi(login) {
    return (users) => {
      if (users.length > 0) return Promise.resolve(users.shift());

      return this._api.find(login);
    };
  }

  _saveOrReturn() {
    return (user) => {
      if (user.isNewRecord !== undefined && !user.isNewRecord) {

        return Promise.resolve(user);
      } else {

        return this._model.create({ login: user.login,
                                    avatar: user.avatar,
                                    blog: user.blog });
      }
    };
  }

  _returns() {
    return (user) => {
      if (user) return Promise.resolve(user);

      return Promise.reject('User not found!');
    };
  }

  findByLogin(login) {
    return this._findOnDb(login)
               .then(this._findOnApi(login))
               .then(this._saveOrReturn())
               .then(this._returns())
               .catch((error) => {
                 return Promise.reject(error);
               });
  }
}

module.exports = User;

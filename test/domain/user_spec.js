var chai           = require('chai'),
    chaiAsPromised = require('chai-as-promised'),
    expect         = chai.expect,
    sinon          = require('sinon'),
    api            = require('../../lib/github/user'),
    models         = require('../../models'),
    model          = models.User,
    UserDomain     = require('../../lib/domain/user');

require('sinon-as-promised');

describe('lib/domain/user', () => {
  beforeEach(() => chai.use(chaiAsPromised));

  describe('#findByLogin', () => {

    describe('when not found', () => {
      var sandbox, apiStub, modelStub, modelCreateStub, subject;

      beforeEach(() => {
        sandbox = sinon.sandbox.create();

        modelStub = sandbox.stub(model, 'findAll'),
        modelStub.returns(Promise.resolve([]));

        modelCreateStub = sandbox.stub(model, 'create'),
        modelCreateStub.returns(Promise.resolve([]));

        apiStub = sandbox.stub(api, 'find'),
        apiStub.returns(Promise.reject('User not found!'));

        subject = new UserDomain(model, api);
      });

      it('returns a fail message', () => {
        return expect(subject.findByLogin('some_login'))
               .to
               .eventually
               .be
               .rejectedWith('User not found!');
      });

      afterEach(() => sandbox.restore());
    });

    describe('when it exists on database', () => {
      var sandbox, modelStub, subject;

      beforeEach(() => {
        sandbox = sinon.sandbox.create();

        modelStub = sandbox.stub(model, 'findAll');
        modelStub.returns(Promise.resolve([{ login: 'some_login',
                                             isNewRecord: false }]));

        subject = new UserDomain(model, api);
      });

      it('returns the User', () => {
        return expect(subject.findByLogin('some_login'))
              .to
              .eventually
              .deep
              .equal({ login: 'some_login',
                       isNewRecord: false });
      });

      afterEach(() => sandbox.restore());
    });

    describe('when not found on database', () => {
      var sandbox, apiStub, modelStub, modelCreateStub, subject;

      beforeEach(() => {
        sandbox = sinon.sandbox.create();

        modelStub = sandbox.stub(model, 'findAll'),
        modelStub.returns(Promise.resolve([]));

        apiStub = sandbox.stub(api, 'find'),
        apiStub.returns(Promise.resolve({ login: 'some_login',
                                          avatar_url: 'avatar_url',
                                          blog: 'blog'}));

        modelCreateStub = sandbox.stub(model, 'create'),
        modelCreateStub.returns(Promise.resolve({ login: 'some_login',
                                                  avatar: 'some_avatar',
                                                  isNewRecord: false}));

        subject = new UserDomain(model, api);
      });

      it('returns the User from Api', () => {
        return expect(subject.findByLogin('some_login'))
              .to
              .eventually
              .deep
              .equal({ login: 'some_login',
                       avatar: 'some_avatar',
                       isNewRecord: false});
      });

      afterEach(() => sandbox.restore());
    });
  });
});
